# Create your views here.
import datetime, string
from django.db.models import Count
from dashboard.models import Discurso, DiscursoTagged, Tag
from django.template import Context, loader
from django.http import HttpResponse
from django.utils.html import strip_tags

def filtro(request, keyword, argument):

    if keyword == "partido":
        queryset = Discurso.objects.filter(partido__sigla=argument)
    elif keyword == "estado":
        queryset = Discurso.objects.filter(estado__sigla=argument)
    elif keyword == "orador":
        queryset = Discurso.objects.filter(orador=argument)
    elif keyword == "texto":
        queryset = Discurso.objects.filter(sumario__contains=argument)
    elif keyword == "tag":
        queryset = Discurso.objects.filter(tags=argument) #bugado
    elif keyword == "last": #bugado
        if argument == "all":
            queryset = Discurso.objects.all()
        else:
            queryset = Discurso.objects.all()[:argument]
    return queryset

def index(request, keyword, argument):
    queryset = filtro(request, keyword, argument)
   
    periodo = {}
    periodo['fim'] = queryset[0].data
    periodo['inicio'] = queryset[len(queryset)-1].data
   
    if request.GET.get("data_inicio"):
        try:
            periodo['inicio'] = datetime.datetime.strptime(request.GET.get("data_inicio"), "%d-%m-%Y") 
            if request.GET.get("data_fim"):
                periodo['fim'] = datetime.datetime.strptime(request.GET.get("data_fim"), "%d-%m-%Y")
            queryset = queryset.filter(data__lte=periodo['fim']).filter(data__gte=periodo['inicio'])
        except:
            return HttpResponse("Data invalida") #criar pagina de erro

    total = queryset.count()
    partidos = queryset.values('partido__sigla').annotate(Count('partido__sigla'))
    for p in partidos:
        p['percent'] = p['partido__sigla__count']*100.0/total

    estados = queryset.values('estado__sigla').annotate(Count('estado__sigla'))
    for e in estados:
        e['percent'] = e['estado__sigla__count']*100.0/total

    oradores = queryset.values('orador', 'orador__nome', 'partido__sigla').annotate(Count('orador__nome'))
    for o in oradores:
        o['percent'] = o['orador__nome__count']*100.0/total

    contagem = queryset.values('discurso', 'data').annotate(Count('data'))
    
    wordcloud = []
    stopwords_list = ['para', 'sobre', 'pelo', 'pela', 'seus', 'suas']
    wholediscurso = ''
    for discurso in queryset:
        wholediscurso += discurso.sumario + ' '
    
    exclude = set(string.punctuation)
    wholediscurso = ''.join(ch for ch in wholediscurso if ch not in exclude)
    
    wholediscurso = wholediscurso.strip("'")
    for word in wholediscurso.split():
        if word not in stopwords_list and len(word) > 3:
            wordcloud.append(word.lower())

        
    t = loader.get_template('dashboard/index.html')
    c = Context({
        'oradores': oradores,
        'estados' : estados,
        'partidos': partidos,
        'discursos': queryset,
        'total': total,
        'contagem': contagem,
        'periodo' : periodo,
        'words' : "['" +"','".join(wordcloud) + "']"
    })
    return HttpResponse(t.render(c))


def taggeia(request):
    #try:
    discurso_id = strip_tags(request.POST.get("id"))
    discurso_tags = strip_tags(request.POST.get("tags", ''))
    q = Discurso.objects.get(pk = discurso_id)
    #except:
    #    return HttpResponse("Error")
    #remove as tags antigas
    dt = DiscursoTagged.objects.filter(discurso=q)
    dt.delete()
    
    #separa tags por virgula, remove caps e whitespace
    for tag_found in discurso_tags.split(","):
        if tag_found:
            try:
                t = Tag.objects.get(tag=tag_found.strip().lower())
            except:
                t = Tag()
                t.tag = tag_found.strip().lower()
                t.save()
            dt = DiscursoTagged()
            dt.discurso = q
            dt.tag = t
            dt.save()
    
    taglist = []
    for tag_placed in DiscursoTagged.objects.filter(discurso=q):
        taglist.append(tag_placed.tag.tag)
    tagstring = ','.join(taglist)
    q.tags = tagstring
    q.save()
    return HttpResponse(tagstring) #corrigir os httpresponses
            
