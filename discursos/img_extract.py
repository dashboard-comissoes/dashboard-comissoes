from lxml.html import parse
import urllib, os, subprocess
import json

def scrapeDate(Datain, txPagina):
    url = "http://imagem.camara.gov.br/dc_20.asp?selCodColecaoCsv=D&txPagina=" + str(txPagina) + '&Datain=' + Datain
    html = urllib.urlopen(url)
    html = parse(html).getroot()
    links = html.cssselect("param")

    jason = {}
    jason['datain'] = Datain
    jason['original_files'] = []
    for link in links:
        if 'http://Imagem.camara.gov.br/Imagem/' in link.get('value'):
            #get and savefile
            page_original_file = {
                'page' : int(link.get('name').strip('page')),
                'url' : link.get('value')
                }
            jason['original_files'].append(page_original_file)
    return jason

def ocrImg(img, ocr):
    subprocess.call(["tesseract", img, ocr[:-4], "-l por"])
    
def downloadImgs(jason):
    current_dir = os.getcwd()
    base_dir = 'archive/'
    date = jason['datain'].split('/')
    date_dir = date[2] + '/' + date[1] + '/' + date[0]
    save_dir = base_dir + date_dir
    if not os.path.isdir(save_dir):
        os.makedirs(save_dir)
    os.chdir(save_dir)
    for index, image in enumerate(jason['original_files']):
        local_img_file = str(image['page']) + '.TIF'
        local_ocr_file = str(image['page']) + '.txt'
        if not os.path.isfile(local_img_file):
            print 'Downloading page: ' + str(image['page'])
            urllib.urlretrieve(image['url'], local_img_file)
            jason['original_files'][index]['img_file'] = local_img_file
        
        if not os.path.isfile(local_ocr_file):
            print "OCRing " + local_img_file
            ocrImg(local_img_file, local_ocr_file)
            jason['original_files'][index]['ocr_file'] = local_ocr_file
                 
    json_file = open('index.json', 'w')
    json_file.write(json.dumps(jason))
    os.chdir(current_dir)
  

jason = scrapeDate('20/03/1963', '699')
downloadImgs(jason)
#ocrImg("/home/markun/devel/esfera/brado-camara/discursos/archive/1963/03/20/8.TIF")
